﻿using System.CommandLine;

namespace ReSharper.Reporting
{
    internal static class Program
    {
        private static int Main(string[] args)
        {
            var root = new RootCommand();

            return root.Invoke(args);
        }
    }
}