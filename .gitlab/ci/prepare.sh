git fetch --all

GIT_VERSION=$(dotnet gitversion | jq -r 'to_entries | map("\(.key)=\(.value)") | .[]')

for value in $GIT_VERSION; do
  export GV_$value
done

export DOTNET_BUILD_OPTS="--configuration Release -p:AssemblyInfo=$GV_AssemblySemVer -p:FileVersion=$GV_AssemblySemFileVer -p:InformationalVersion=$GV_InformationalVersion"

echo DOTNET_BUILD_OPTS="$DOTNET_BUILD_OPTS"